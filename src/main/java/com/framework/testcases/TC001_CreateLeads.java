package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;

public class TC001_CreateLeads extends ProjectMethods {
@BeforeTest
public void setData()
{
	testCaseName="TC001_CreateLeads";
	testDescription="Create Leads";
	testNodes="Leads";
	author="ramya";
	category="smoke";
	dataSheetName="TC001";
}
@Test(dataProvider="fetchData")
public void login(String Uname, String Pwd, String Cname, String Fname, String Lname )
{
	new LoginPage()
	.enterUsername(Uname)
	.enterPassword(Pwd)
	.clickLogin()
	.clickCRM_SFA()
	.clickleads()
	.ClickCreateLead()
	.EnterCName(Cname)
	.EnterFname(Fname)
	.EnterLname(Lname)
	.clickCreateLeadButton()
	.VerifyFirstName();
	
	
}

}
