package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID,using="createLeadForm_companyName")WebElement eleCname;
	@FindBy(how=How.ID,using="createLeadForm_firstName")WebElement eleFname;
	@FindBy(how=How.ID,using="createLeadForm_lastName")WebElement eleLname;
	@FindBy(how=How.XPATH,using="//input[@name='submitButton']")WebElement elecreateleadclick;
	public CreateLeadPage EnterCName(String Data){
		clearAndType(eleCname, Data);
		return this;
		
	}
	public CreateLeadPage EnterFname(String Data)
	{
	
		clearAndType(eleFname, Data);
		return this;
	}
	public CreateLeadPage EnterLname(String Data)
	{
	
		clearAndType(eleLname, Data);
		return this;
	}
public ViewLeadPage clickCreateLeadButton() {
	click(elecreateleadclick);
	return new ViewLeadPage();
	
}
}
