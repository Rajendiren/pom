package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeadsPage  extends ProjectMethods{
	public  MyLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy (how =How.LINK_TEXT, using="Create Lead")WebElement CreateLead;
	public  CreateLeadPage ClickCreateLead() {
		//WebElement CreateLeads;
		click (CreateLead);
		return new CreateLeadPage();
		
	}
}
